" Plugins
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'fatih/vim-go'
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/NERDTree'
call vundle#end()
filetype plugin indent on

let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_fields = 1
let g:go_highlight_types = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1

set autowrite

autocmd FileType go nmap <leader>n :cnext<CR>
autocmd FileType go nmap <leader>p :cprevious<CR>
autocmd FileType go nmap <leader>b <Plug>(go-build)
autocmd FileType go nmap <leader>r <Plug>(go-run)
autocmd FileType go nmap <leader>t <Plug>(go-test)
autocmd FileType go nmap <leader>l :GoLint<CR>
autocmd FileType go nmap <leader>a :GoCheckAll<CR>

" autocmd BufWritePre *.go :GoBuild

function! GoCheckAll()
  :GoBuild<CR>
  :GoLint<CR>
  :GoTest<CR>
endfunction
command! GoCheckAll :call GoCheckAll()

let g:NERDTreeHijackNetrw=1
autocmd VimEnter * NERDTree
autocmd BufWinEnter * NERDTreeMirror
autocmd VimEnter * wincmd w
nmap <leader>o :NERDTreeToggle<CR>


" Auto save files on window blur
" autocmd! FocusLost * :silent! up

" make and python use real tabs
"au! FileType make    set noexpandtab

"au! FileType scss    syntax cluster sassCssAttributes add=@cssColors

" Thorfile, Rakefile and Gemfile are Ruby
au! BufRead,BufNewFile {Gemfile,Rakefile,Thorfile,config.ru}    set ft=ruby

au! BufRead,BufNewFile gitconfig set ft=gitconfig

syntax on

set t_Co=256

" Visual
  set number
  set ruler
  set guioptions=ce
  set showmatch                 " Briefly jump to a paren once it's balanced
  set linespace=2
  set background=dark
  colors oceanblack256

" Tabs/Whitespace
  set tabstop=2
  set shiftwidth=2
  set autoindent
  set smarttab
  set expandtab
  set nowrap
  set list
"  set listchars=tab:▸\ ,eol:¬,trail:·
  set backspace=indent,eol,start " allow backspacing over everything in insert mode
  set invlist
" Misc
  set shell=bash
  set nocompatible
" set paste
  set switchbuf=useopen         " Don't re-open already opened buffers
  set nostartofline             " Avoid moving cursor to BOL when jumping around
  set virtualedit=all           " Let cursor move past the last char
  set whichwrap=b,s,h,l,<,>,[,]
"  let mapleader = ','
  set autoread                  " watch for file changes
"  set mouse=a                  " I don't like the mouse on laptops
  set fileformats=unix

" Bells
  set novisualbell  " No blinking
  set noerrorbells  " No noise.
  set vb t_vb= " disable any beeps or flashes on error

" Searching
  set hlsearch
  set incsearch
  set ignorecase
  set smartcase

" Tab completion
  set wildmode=list:longest,list:full
  set wildignore+=*.o,*.obj,.git,*.rbc,*.swp

" Status bar
  set laststatus=2
  set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P

" Folding
  set foldenable " Turn on folding
  set foldmethod=marker " Fold on the marker
  set foldlevel=100 " Don't autofold anything (but I can still fold manually)
  set foldopen=block,hor,mark,percent,quickfix,tag " what movements open folds

  set backupdir=~/.vim/dirs/backups
  set directory=~/.vim/dirs/swaps

" Nicer text navigation
  nmap j gj
  nmap k gk

" Shut up about ex mode
  noremap Q <NOP>

" Reselect visual block after adjusting indentation
  vnoremap < <gv
  vnoremap > >gv

" Nicer splitting
  set splitbelow
  set splitright
  map <C-_> :split<CR>
  map <C-\> :vsplit<CR>

" Emacs-like keys for the command line
  cnoremap <C-A> <Home>
  cnoremap <C-E> <End>
  cnoremap <C-K> <C-U>

" Move around in insert mode
  inoremap <C-h> <left>
  inoremap <C-k> <up>
  inoremap <C-j> <down>
  inoremap <C-l> <right>

" White border annoys me
  hi StatusLine ctermbg=0 ctermfg=2
  hi StatusLineNC ctermbg=0 ctermfg=3

" Opens an edit command with the path of the currently edited file filled in Normal mode: <Leader>e
  map <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>

" Strip trailing whitespace on save
  function! <SID>StripTrailingWhitespaces()
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    %s/\s\+$//e
    " Clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
  endfunction
  autocmd! BufWritePre * :call <SID>StripTrailingWhitespaces()

" Template for rendering HTML
  function! RenderHtmlTemplate()
    normal! Go  </body>
    normal! Go</html>
    normal! ggO  <body>
    normal! ggO  </head>
    normal! ggO    <title></title>
    normal! ggO  <head>
    normal! ggO<html>
    normal! ggO<!DOCTYPE html>
    call cursor(4,12)
  endfunction
  command! RenderHtmlTemplate :call RenderHtmlTemplate()
