base=$(dirname $HOME/$(readlink $BASH_SOURCE))

if [ -d "$HOME/.bin" ]
then
  export PATH=$HOME/.bin:$PATH
fi

if [ -d "$base/bin" ]
then
  export PATH="$base/bin:$PATH"
fi

if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

if [ -d "$HOME/.bash_completion.d" ]; then
  for f in $HOME/.bash_completion.d/*; do
    . $f
  done
fi

for src in $(ls $base/src)
do
    source $base/src/$src
done
for plugin in $(ls $base/plugins)
do
    source $base/plugins/$plugin
done
